module gitlab.com/grumblechat/server

go 1.16

require (
	github.com/getsentry/sentry-go v0.10.0
	github.com/go-playground/validator/v10 v10.6.1
	github.com/jinzhu/configor v1.2.1
	github.com/jinzhu/copier v0.3.0
	github.com/labstack/echo/v4 v4.2.2
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/matoous/go-nanoid/v2 v2.0.0 // indirect
	github.com/segmentio/ksuid v1.0.3
	go.etcd.io/bbolt v1.3.5
)
